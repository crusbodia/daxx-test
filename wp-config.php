<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '1111');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}a~gP7#q8:}c87[-&pi5wBpV$9{m&q_nhP&i+yF.QZ>F.(>kw6tqjo{_72w-Z`?X');
define('SECURE_AUTH_KEY',  ';{8`go7KL;TYQA)2<8149@biLeOex+~$dqD84r,#(gD~.-@{`-G#0WhiO#-90gI>');
define('LOGGED_IN_KEY',    't;{=!t^1frJ^2ju@+@^@b4CorvKiz&l-0U&bvEK0R~XHx>#>;;wl6$-^2o$fW0JF');
define('NONCE_KEY',        '^R&&ygB3/#I>PQx0vj{0zV#(!EBxn5es0Jlm3%Vd(Q0ZxF+)U_,cIU5EJgl9`*~[');
define('AUTH_SALT',        'cM^tD]U@dL#b8h0^xN?i56u7; _I|rFicdROIXjyAnIwGlfqDPol(a9 Um) ?{M8');
define('SECURE_AUTH_SALT', '{1O+3=Y9:p=^1=v3V8BdQKF(JdH^)&gVjT6M;,|,0>2.5Bzox96e/Eq(+*]g& $,');
define('LOGGED_IN_SALT',   'aINA=;$Rm_VxqzEK/^ncm6Q8qeI_rWvu!Fr6)LVWewNY%uxR1zU} }B{dV_/C4HZ');
define('NONCE_SALT',       '(^xgXL3gD0j?ap,G:$Q$>[vlV)/1U!($P9 ,Hm%diIr@U=?0ltW>v|gYEMpEGo?G');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
