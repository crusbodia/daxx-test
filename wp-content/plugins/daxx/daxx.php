<?php
/*
Plugin Name: Daxx
Version: 1.0
*/

function daxx_add_settings_section () {
    if ( false == get_option( 'daxx_options' ) ) {
        add_option( 'daxx_options' );
    }

    add_settings_section(
        'general_setting_section',
        'Settings Daxx plugin',
        'daxx_settings_section',
        'daxx_options'
    );

    add_settings_field(
        'items',
        'Items',
        'daxx_settings_field_items',
        'daxx_options',
        'general_setting_section'
    );

    register_setting(
        'daxx_options',
        'daxx_options',
        'items_valid_data'
    );

}

function daxx_admin_settings() {
    add_plugins_page(
        'Daxx plugin settings',
        'Daxx plugin settings',
        'administrator',
        'daxx_options',
        'daxx_options_page'
    );
}

function daxx_options_page() { ?>
    <div class="wrap">
        <!-- <div id="icon-themes" class="icon32"></div>  -->
        <h2>Daxx options</h2>
        <?php settings_errors(); ?>

        <form method="post" action="options.php">
            <?php settings_fields( 'daxx_options' );
            do_settings_sections( 'daxx_options' );
            submit_button(); ?>
        </form>
    </div>
<?php }

add_action( 'admin_init', 'daxx_add_settings_section' );
add_action( 'admin_menu', 'daxx_admin_settings' );

function daxx_settings_section() {
    echo "Add shortcode ['daxx_shortcode'] in content to show data on page";
}

function daxx_settings_field_items() {
    $options = get_option( 'daxx_options' );

    if (is_array($options) && isset($options['items']) && ! empty($options['items'])) {
        $items = $options['items'];
    } else {
        $items = -1;
    } ?>
    <label> Items to show (-1 for all items): </label>
    <input type="text" name="daxx_options[items]" id="items" value="<?php echo $items; ?>" />
<?php }

function items_valid_data($data) {
    $output_data = array();
    if (is_array($data)) {
        foreach ( $data as $key => $value ) {
            if ( isset( $data[$key] ) ) {
                $output_data[$key] = trim(strip_tags(stripslashes($data[$key])));
            }
        }
    }

    $output_data['items'] =  ( int ) $output_data['items'];
    $output_data['items'] = ($output_data['items'] < -1) ? -1 : $output_data['items'];
    return apply_filters( 'items_valid_data', $output_data, $data );
}

function daxx_shortcode_function() {
    $options = get_option( 'daxx_options' );
    if (isset ($options['items']) && ! empty($options['items'])) {
        $items = $options['items'];
    } else {
        $items = -1;
    }

    $response = get_transient( 'daxx_user_' . $items );

    if( false === $response ) {
        $response = wp_remote_get( 'https://jsonplaceholder.typicode.com/users' );
        set_transient( 'daxx_user_' . $items, $response, 60*60*24 );
    }

    $data = json_decode( $response['body'], true );
    if (! is_array($data)) {
        return "No data";
    }

    $i = 0;
    foreach ($data as $item) :
        $i++;
        if ($items != -1) {
            if ($i > $items) {
                break;
            }
        } ?>
        <div class="item-wrap user-id-<?php echo isset($item['id']) ? $item['id'] : ''; ?>">
            <h1><?php echo isset($item['name']) ? $item['name'] : ''; ?></h1>
            <div class="username">
                <span class="user-label">Username: </span>
                <?php echo isset($item['username']) ? $item['username'] : ''; ?>
            </div>
            <div class="email">
                <span class="user-label">Email: </span>
                <?php echo isset($item['email']) ? $item['email'] : ''; ?>
            </div>
            <div class="phone">
                <span class="user-label">Phone: </span>
                <?php if (isset($item['phone'])) { ?>
                    <a href="tel:<?php echo $item['phone']; ?>"><?php echo $item['phone']; ?></a>
                <?php } ?>
            </div>
            <div class="website">
                <span class="user-label">Website: </span>
                <?php if (isset($item['website'])) { ?>
                    <a href="<?php echo $item['website']; ?>"><?php echo $item['website']; ?></a>
                <?php } else {
                    echo "No website";
                } ?>
            </div>
            <div class="address">
                <span class="user-label">Address: </span>
                <?php echo isset($item['address']['street']) ? $item['address']['street'] . ', ' : ''; ?>
                <?php echo isset($item['address']['suite']) ? $item['address']['suite'] . ', ' : ''; ?>
                <?php echo isset($item['address']['city']) ? $item['address']['city'] : ''; ?>
                <br /><?php echo isset($item['address']['zipcode']) ? $item['address']['zipcode'] . '<br />' : ''; ?>
                Coords: <?php echo isset($item['address']['geo']['lat']) ? $item['address']['geo']['lat'] : ''; ?>,
                <?php echo isset($item['address']['geo']['lng']) ? $item['address']['geo']['lng'] : ''; ?>
            </div>
            <div class="company">
                <span class="user-label">Company: </span>
                <?php echo isset($item['company']['name']) ? $item['company']['name'] . ', ' : ''; ?>
                <?php echo isset($item['company']['catchPhrase']) ? $item['company']['catchPhrase'] . ', ' : ''; ?>
                <?php echo isset($item['company']['bs']) ? $item['company']['bs'] : ''; ?>
            </div>
        </div>
    <?php endforeach;

}

add_shortcode('daxx_shortcode', 'daxx_shortcode_function');

add_filter('the_content', 'do_shortcode', 11);