<?php

$timber = new \Timber\Timber();


class Daxx extends TimberSite {

    function __construct() {
        add_theme_support( 'post-formats' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'menus' );
        add_filter( 'timber_context', array( $this, 'add_to_context' ) );
        add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
        add_action( 'init', array( $this, 'register_post_types' ) );
        add_action( 'init', array( $this, 'register_taxonomies' ) );

        add_action( 'wp_enqueue_scripts', array( $this, 'scripts_styles' ) );
        parent::__construct();
    }

    function register_post_types() {
        $args = array(
            'label'  => null,
            'labels' => array(
                'name'               => 'Coach',
                'singular_name'      => 'Coach',
                'add_new'            => 'Add coach',
                'add_new_item'       => 'Add new coach',
                'edit_item'          => 'Edit coach',
                'new_item'           => 'New coach',
                'view_item'          => 'View coach',
                'menu_name'          => 'Coaches'
            ),
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => 4,
            'menu_icon'           => 'dashicons-businessman',
            'hierarchical'        => false,
            'supports'            => array('title','editor', 'thumbnail'),
            'taxonomies'          => array('type', 'loc'),
            'has_archive'         => true,
            'rewrite'             => true,
            'query_var'           => true,
            'show_in_nav_menus'   => null,
        );

        register_post_type('coach', $args );
    }

    function register_taxonomies() {
        register_taxonomy('type', array('coach'), array(
            'labels' => array(
                'name'              => 'Types',
                'singular_name'     => 'Coach type',
            ),
            'hierarchical' => true,
            'show_ui' => true,
            'query_var' => true
        ));
        register_taxonomy('loc', array('coach'), array(
            'labels' => array(
                'name'              => 'Location',
                'singular_name'     => 'Location',
            ),
            'hierarchical' => true,
            'show_ui' => true,
            'query_var' => true
        ));
    }

    function add_to_context( $context ) {
        $context['foo'] = 'bar';
        $context['stuff'] = 'I am a value set in your functions.php file';
        $context['notes'] = 'These values are available everytime you call Timber::get_context();';
        $context['menu_top'] = new TimberMenu('top');
        $context['menu_primary'] = new TimberMenu('primary');
        $context['menu_secondary'] = new TimberMenu('secondary');
        $context['site'] = $this;
        return $context;
    }

    function myfoo( $text )
    {
        $text .= ' bar!';
        return $text;
    }

    function add_to_twig( $twig )
    {
        /* this is where you can add your own fuctions to twig */
        $twig->addExtension( new Twig_Extension_StringLoader() );
        $twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
        return $twig;
    }

    public function scripts_styles()
    {

        wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/lib/owl.carousel/owl.carousel.min.js', array('jquery'));
        wp_enqueue_script('main-script', get_template_directory_uri() . '/js/script.js', array('jquery', 'owl-carousel'));

        wp_enqueue_style('bootstrap', get_template_directory_uri() . '/lib/bootstrap/css/bootstrap.min.css');
        wp_enqueue_style('font-awesome', get_template_directory_uri() . '/lib/font-awesome/css/font-awesome.min.css');
        wp_enqueue_style('owl-style', get_template_directory_uri() . '/lib/owl.carousel/assets/owl.carousel.min.css');
        wp_enqueue_style('daxx_styles', get_template_directory_uri() . '/css/styles.css');
    }

}
new Daxx();

function send_form() {
    $values = array();
    parse_str($_POST['form'], $values);

    $msg = array('', true);

    if (! isset($values['email']) || empty($values['email'])) {
        $msg = array('Email is required', false);
    } else if (!filter_var($values['email'], FILTER_VALIDATE_EMAIL)) {
        $msg = array('Invalid email', false);
    }
    if (! isset($values['name']) || empty($values['name'])) {
        $msg = array('Name is required', false);
    }
    if (! isset($values['message']) || empty($values['message'])) {
        $msg = array('Message is required', false);
    }
    if ($values['clicked'] !== 'human') {
        $msg = array('Sorry, try again', false);
    }
    if ($msg[1] === false) {
        echo json_encode($msg);
        exit();
    }

    $subject = "Someone sent a message from ".get_bloginfo('name');
    $headers = 'From: '. $values['email'] . "\r\n" .
        'Reply-To: ' . $values['email'] . "\r\n";

    $is_send = wp_mail($values['email'], $subject, $values['message'], $headers );

    if (! $is_send) {
        $msg = array('Email didn\'t send', false);
    } else {
        $msg = array('Email sent', true);
    }

    echo json_encode($msg);
    exit();
}

add_action('wp_ajax_sendForm', 'send_form');
add_action('wp_ajax_nopriv_sendForm', 'send_form');

add_action( 'init', 'blockusers_init' );
function blockusers_init() {
    if ( is_admin() && ! current_user_can( 'administrator' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
        wp_redirect( home_url() );
        exit;
    }
}

function redirect_login_page() {
    $page_viewed = basename($_SERVER['REQUEST_URI']);

    if( $page_viewed == "wp-login.php?pass=1" ) {
        wp_redirect( home_url() );
        exit;
    }
}
add_action('init','redirect_login_page');

function logout_page() {
    $login_page  = home_url( 'wp-admin' );
    wp_redirect( $login_page . "?loggedout=true" );
    exit;
}
add_action('wp_logout','logout_page');