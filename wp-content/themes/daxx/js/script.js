jQuery(document).ready(function($) {
    $(".owl-carousel").owlCarousel({
        items: 1,
        dots: false,
        nav:true,
        // autoHeight:true,
        // loop:true,
        navText: [
            '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'
        ]
    });


    var $form = $('#contact-form');
    var $clicked = $form.find('.clicked');
    var $formResponse = $('.form-response');
    var inProcess = false;
    $form.find('.name').on('click', function () {
        $clicked.val('human');
    });
    $form.submit(function (e) {
        e.preventDefault();
        if ($clicked.val() !== 'human') {
            $formResponse.text('Sorry, try again').removeClass('success-f').addClass('error-f');
            return false;
        }
        var form;
        form = $(this).serialize();

        $.ajax({
            type:"POST",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'sendForm',
                form: form
            },
            beforeSend: function () {
                if (inProcess)
                    return false;
                inProcess = true;
            },
            success: function(results){
                var resArr = JSON.parse(results);
                $formResponse.text(resArr[0]);
                if (resArr[1]) {
                    $formResponse.addClass('success-f').removeClass('error-f');
                } else {
                    $formResponse.removeClass('success-f').addClass('error-f');
                }
            },
            error: function () {
                $formResponse.text('Sorry, try again').removeClass('success-f').addClass('error-f');
            },
            complete: function () {
                inProcess = false;
            }
        });
    });

    $('.img-wrap').on('hover', function () {
        $(this).find('.coach-link').show();
    }).on('mouseleave', function () {
        $(this).find('.coach-link').hide();
    });

    function filterCoaches() {
        var type = $filterType.val();
        var loc = $filterLoc.val();
        var search = $filterSearch.val();

        var flType = '',
            flLoc = '',
            flSearch = '';

        var $coachItem = $('.coach-item-wrap');
        if (type == '' && loc == '' && search == '') {
            $coachItem.show();
            return;
        }
        if (type != '') {
            flType = '.fl-' + type;
        }
        if (loc != '') {
            flLoc = '.fl-' + loc;
        }
        $coachItem.hide().addClass('coach-hide');
        $(flType + flLoc).show().addClass('coach-show');
        if ( search != '') {
            if (type == '' && loc == '') {
                $coachItem.addClass('coach-show').removeClass('coach-hide');
            }
            $coachItem.hide();
            var $coachShowArr = $('.coach-show');
            $.each($coachShowArr, function () {
                if ($(this).data('name').toLowerCase().indexOf(search.toLowerCase()) != -1) {
                    $(this).show().addClass('coach-show').removeClass('coach-hide');
                } else {
                    $(this).hide().addClass('coach-hide').removeClass('coach-show');
                }
                a = 'asfg';
            });
        }
    }

    if ($('.coach-archive').length > 0) {
        var $filterType = $('#filter-type');
        var $filterLoc = $('#filter-loc');
        var $filterSearch = $('#search-coach');

        $filterType.on('change', filterCoaches);
        $filterLoc.on('change', filterCoaches);
        $filterSearch.on('change', filterCoaches);
    }

});