<?php
//$posts = Timber::query_post('post_type=coach&numberposts=2');
//$context['fields'] = get_fields();
/**
 * The template for displaying Archive pages.
 *
 */
$context['fields'] = get_fields();
$templates = array( 'archive-coach.twig', 'archive.twig', 'index.twig' );
$context = Timber::get_context();

$context['posts'] = Timber::get_posts('post_type=coach&numberposts=-1');
$context['type'] = Timber::get_terms('type');
$context['loc'] = Timber::get_terms('loc');

Timber::render( $templates, $context );
