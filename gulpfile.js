require('es6-promise').polyfill();

var gulp = require('gulp'),
    // browserSync = require('browser-sync'),
    sass = require('gulp-sass'),
    prefix = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber'),
    // cssShorthand = require('gulp-shorthand'),
    minifyCss = require('gulp-minify-css'),
    sourcemaps = require('gulp-sourcemaps');

//  wait = require('gulp-wait');
//upload = require('gulp-upload'),
// shell = require('gulp-shell');

//var sftp = require('gulp-sftp');
//  var ftp = require('gulp-ftp');
//var sftp = require('gulp-sftp');
//var path = require('path');

var theme_path = 'wp-content/themes/daxx/';



gulp.task('sass', function () {
    return gulp.src([theme_path + 'scss/styles.scss']) // the source .scss file
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass()) // pass the file through gulp-sass
        //.pipe(cssShorthand())
        .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(theme_path + 'css')); // output .css file to css folder;
        //.pipe(wait(1500))
        //.pipe(browserSync.reload({stream: true})) // reload the stream
});

// gulp.task('reload', function () {
//     browserSync.reload();
// });


gulp.task('watch', function () {
    gulp.watch([theme_path + 'scss/*.scss'], ['sass']);
    // gulp.watch(theme_path + '**/*.{php,inc,js}', ['reload']);

});

gulp.task('browser-sync', ['sass'], function () {
    browserSync.init({
        port: 3304,
        // tunnel: true
        //watchTask: true,
        proxy: "wp.loc/"
        //injectChanges: true,
        // server: false
        // reloadDelay: 2000
        // files: [theme_path + "css/*.css"]
        /*socket: {
         // For local development only use the default Browsersync local URL.
         domain: '127.0.0.1:3000'
         }*/
    });
});

gulp.task('default', ['watch']);
// gulp.task('default', ['browser-sync', 'watch']);